<?php
/**
 * @author Baptiste MOINE <contact@bmoine.fr>
 * @project Appli1-CAS
 * @homepage http://appli1.miletrie.lan/
 * @released 06/01/2017
 */

//! Debug
error_reporting(E_ALL);
ini_set("display_errors", "On");

//! Constantes
define("BASE_DIR", dirname(__FILE__));   //!< Définit le répertoire racine du projet
define("DS", DIRECTORY_SEPARATOR);       //!< Définit le séparateur de répertoire en fonction de la configuration du système hôte
define("CAS_HOST", "cas.miletrie.lan");
define("CAS_PORT", 443);
define("CAS_URI", "cas");
define("CAS_START_SESSION", false);

setlocale(LC_TIME, 'fr_FR.utf8', 'fra'); //!< Définit les paramètres régionaux et linguistiques

//! Affichage d'une erreur à l'écran
$fatalError = function ($e) {
    if ( ! headers_sent() ) {  //!< En-tête HTTP non envoyée
        header("500 Internal Server Error; Content-Type: text/html; charset=utf-8", true, 500);  //!< Remplace le code de status par 500 (Erreur interne au serveur)
    }
    echo "<p>" . $e->getMessage() . "</p>";
    echo "<p>Une erreur fatale est survenue pendant le traitement de la requête. Veuillez <a href=\"#\" title=\"Recharger la page\" onclick=\"window.location.reload(true); return false;\">rafraichir la page</a> et réessayer.</p>";
    exit(1);
};

try {
//! Chargement de phpCAS
    $casLoader = BASE_DIR . DS . "vendor" . DS . "phpCAS" . DS . "CAS.php";

    if ( file_exists($casLoader) && is_readable($casLoader) ) {
        require $casLoader;
    } else {
        throw new \Exception("CAS n'existe pas ou n'est pas accessible en lecture.");
    }

//! Initialisation de CAS et traitement de l'authentification utilisateur
    phpCAS::setDebug();
    phpCAS::setVerbose(true);
    phpCAS::client(SAML_VERSION_1_1, CAS_HOST, CAS_PORT, CAS_URI, CAS_START_SESSION);
    phpCAS::handleLogoutRequests();      //!< Traitement des demandes de déconnexion
    phpCAS::setNoCasServerValidation();  //!< Aucune validation du certificat serveur
    phpCAS::forceAuthentication();       //!< Force l'authentification
    if (isset($_GET['logout'])) {
        phpCAS::logout();                //!< Désauthentification de l'utilisateur
    }
} catch (\Exception $e) {
    $fatalError($e);  //!< Affichage d'une erreur à l'écran
}

//! Préparation du contenu
    ob_start();                   //!< Initialisation du tampon (buffer)
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr" itemscope="itemscope" itemtype="http://schema.org/QAPage">
    <head>
        <!-- BEGIN Metadata -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <meta name="language" content="fr-fr" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, target-densitydpi=device-dpi" />
        <meta name="application-name" content="Application 1" />
        <meta name="distribution" content="global" />
        <meta name="robots" content="index, follow" />

        <meta name="contact" content="contact@bmoine.fr" />
        <meta name="author" content="Baptiste MOINE" />
        <link rel="author" href="https://www.bmoine.fr/" />
        <link rel="publisher" href="https://www.bmoine.fr/" />

        <title>Application 1</title>
        <!-- END Metadata -->

        <!-- BEGIN Stylesheets -->
        <link rel="stylesheet" media="all" href="/assets/css/core.css" />
        <link rel="stylesheet" media="all" href="/assets/css/app.css" />
        <!-- END Stylesheets -->

        <!-- BEGIN Scripts -->
        <script type="text/javascript" src="/assets/js/lib/jquery-3.1.1.min.js"></script>
        <!--[if lt IE 10]>
        <div id="upgrade-ie">
            <div class="wrapper">
                <div class="row">
                    <p>Vous utilisez une version obsolète du navigateur Internet Explorer.</p>
                    <p>Pour naviguer sur l'Internet de manière rapide et plus sécurisée, veuillez prendre le temps nécessaire pour <a href="https://www.whatbrowser.org/intl/fr/" title="Mettre à jour son navigateur" target="_blank">mettre à jour votre navigateur</a>.</p>
                </div>
            </div>
        </div>
        <![endif]-->
        <!--[if lt IE 9]>
        <script type="text/javascript" src="/js/lib/html5shiv.js"></script>
        <![endif]-->
        <!-- END Scripts -->
    </head>
    <body id="top" itemscope="itemscope" itemtype="http://schema.org/WebPage" role="document">
        <header id="page-header" role="banner">
            <div class="wrapper">
              <div class="row">
                <div class="col-l-6 col-m-6 col-s-6">
                  <h1 itemprop="name" id="page-title">Application 1</h1>
                </div><!--
                --><div class="col-l-6 col-m-6 col-s-6 right">
                  <a href="/?logout" title="Se déconnecter">Se déconnecter</a>
                </div>
              </div>
            </div>
        </header>
        <main id="site-content" role="main">
            <div class="wrapper">
                <div class="row">
                    <div class="col-l-12 col-m-12 col-s-12">
                        <p>Bonjour <b><?php echo phpCAS::getUser(); ?></b>.</p>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>
<?php
    $buffer = ob_get_contents();  //!< Sauvegarde du buffer
    ob_end_clean();               //!< Vidage du buffer
    echo $buffer;                 //!< Affichage du buffer final
